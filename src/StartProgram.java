import GeomFigures.Angular.Polygon;
import GeomFigures.Angular.Rhombus;
import GeomFigures.Angular.Square;
import GeomFigures.NonAngular.Circle;
import GeomFigures.Point;

import java.util.ArrayList;

public class StartProgram {
    public static void main(String[] args) {
        Circle circle = new Circle(3, 0, 0, "Black");
        System.out.println("The circle's area is " + circle.getArea());
        System.out.println("The circle's perimeter is " + circle.getPerimeter());
        System.out.println(circle);
        System.out.println();

        Square square = new Square(5, 1, 1, "Orange");
        System.out.println("The square's area is " + square.getArea());
        System.out.println("The square's perimeter is " +square.getPerimeter());
        System.out.println(square);
        square.output();
        System.out.println();

        ArrayList<Point> abc = new ArrayList<>();
        abc.add(new Point(1, 2));
        abc.add(new Point(2, 3));
        abc.add(new Point(5, 3));

        Polygon polygon = new Polygon(abc, "Green");
        System.out.println("The polygon's area is " + polygon.getArea());
        System.out.println("The polygon's perimeter is " + polygon.getPerimeter());
        System.out.println(polygon);
        polygon.output();
        System.out.println();

        ArrayList<Point> abcd = new ArrayList<>();
        abcd.add(new Point(0, 4));
        abcd.add(new Point(6, 8));
        abcd.add(new Point(12, 4));
        abcd.add(new Point(6, 0));

        Rhombus rhombus = new Rhombus(abcd, "Beige");
        System.out.println("The rhombus area is " + rhombus.getArea());
        System.out.println("The rhombus perimeter is " + rhombus.getPerimeter());
        System.out.println(rhombus);
        rhombus.output();
    }
}