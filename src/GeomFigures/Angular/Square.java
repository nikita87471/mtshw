package GeomFigures.Angular;

import GeomFigures.GeomFigure;
import GeomFigures.Point;

public class Square extends GeomFigure implements WithAngles{   //Квадрат
    private double squareSideLength;

    private Point start;

    private String color;

    public Square(double squareSideLength, double pointX, double pointY, String color) {
        this.squareSideLength = squareSideLength;
        this.start = new Point(pointX, pointY);
        this.color = color;
    }

    @Override
    public double getPerimeter() {
        return squareSideLength *4;
    }

    @Override
    public double getArea() {
        return squareSideLength * squareSideLength;
    }

    public void setColor(String new_color) {
        this.color = new_color;
    }


    @Override
    public String toString() {
        return "Square{" +
                "st=" + squareSideLength +
                ", pointX=" + start.getX() +
                ", pointY=" + start.getY() +
                ", color='" + color + '\'' +
                '}';
    }

    @Override
    public void output() {
        System.out.println("The square's coordinates are:");
        System.out.println(start.getX() + " " + start.getY());
        System.out.println(Double.toString(start.getX() + squareSideLength) + " " + start.getY());
        System.out.println((start.getX() + " " + Double.toString(start.getY() + squareSideLength)));
        System.out.println(Double.toString(start.getX() + squareSideLength) + " " + Double.toString(start.getY() + squareSideLength));
    }
}