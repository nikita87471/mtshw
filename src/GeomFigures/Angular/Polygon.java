package GeomFigures.Angular;

import GeomFigures.GeomFigure;
import GeomFigures.Point;

import java.util.ArrayList;
import java.util.List;

public class Polygon extends GeomFigure implements WithAngles { //многоугольник
    private List<Point> listOfPoints;

    private String color;

    @Override
    public double getPerimeter() {
        double dist = 0;
        for (int i = 0; i < listOfPoints.size()-1; i++){
            dist = dist + Math.sqrt(Math.pow(listOfPoints.get(i+1).getX() - listOfPoints.get(i).getX(), 2) +
                    Math.pow(listOfPoints.get(i+1).getY() - listOfPoints.get(i).getY(), 2));
        }
        dist = dist + Math.sqrt(Math.pow(listOfPoints.get(listOfPoints.size()-1).getX() - listOfPoints.get(0).getX(), 2) +
                Math.pow(listOfPoints.get(listOfPoints.size()-1).getY() - listOfPoints.get(0).getY(), 2));
        return dist;
    }

    @Override
    public double getArea() {
        double temp_sum_1=0;
        double temp_sum_2=0;
        for (int i=0; i<listOfPoints.size(); i++){
            temp_sum_1+=listOfPoints.get(i).getX()*listOfPoints.get((i+1)% listOfPoints.size()).getY();
            temp_sum_2+=listOfPoints.get((i+1)%listOfPoints.size()).getX()*listOfPoints.get(i).getY();
        }
        return 0.5*Math.abs(temp_sum_1-temp_sum_2);
    }
    public void setColor(String new_color) {
        this.color = new_color;
    }
    public String getColor(){
        return this.color;
    }
    @Override
    public void output() {
        System.out.println("The polygon's coordinates are:");
        for (int i = 0; i < listOfPoints.size(); i++){
            System.out.println(Double.toString(listOfPoints.get(i).getX()) + " " + Double.toString(listOfPoints.get(i).getY()));
        }
    }

    public Polygon(ArrayList<Point> list, String color) {
        this.listOfPoints = list;
        this.color = color;
    }

    @Override
    public String toString() {
        return "Polygon{" +
                "color='" + color + '\'' +
                '}';
    }
}

