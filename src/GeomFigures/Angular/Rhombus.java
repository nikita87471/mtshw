package GeomFigures.Angular;

import GeomFigures.Point;

import java.util.ArrayList;

public class Rhombus extends Polygon implements Tetragon{

    private double rhombusSideLength = 0;

    private double rhombusSmallDiagLength = 0;

    private double rhombusBigDiagLength = 0;

    private Point rhombusCenter;

    public void calculateParams(ArrayList<Point> list){
        this.rhombusSideLength = Math.sqrt(Math.pow(list.get(0).getX() - list.get(1).getX(), 2) + Math.pow(list.get(0).getY() - list.get(1).getY(), 2));
        this.rhombusSmallDiagLength = Math.sqrt(Math.pow(list.get(1).getX() - list.get(3).getX(), 2) + Math.pow(list.get(1).getY() - list.get(3).getY(), 2));
        this.rhombusBigDiagLength = Math.sqrt(Math.pow(list.get(0).getX() - list.get(2).getX(), 2) + Math.pow(list.get(0).getY() - list.get(2).getY(), 2));
        this.rhombusCenter = new Point (0.5*(Math.sqrt(Math.pow(list.get(0).getX() - list.get(2).getX(), 2) + Math.pow(list.get(0).getY() - list.get(2).getY(), 2))), list.get(0).getY());

    }

    public Rhombus(ArrayList<Point> list, String color) {
        super(list, color);
        if (list.size() == angleQuantity){
           this.calculateParams(list);
        }
        else{
            System.out.println("The rhombus must have 4 points!");
        }
    }


    @Override
    public void output() {
        super.output();
    }

    @Override
    public String toString() {
        return "Rhombus{" +
                "rhombusSideLength=" + rhombusSideLength +
                ", rhombusSmallDiagLength=" + rhombusSmallDiagLength +
                ", rhombusBigDiagLength=" + rhombusBigDiagLength +
                ", rhombusCenterX=" + rhombusCenter.getX() +
                ", rhombusCenterY=" + rhombusCenter.getY() +
                ", color='" + getColor() + '\'' +
                '}';
    }
}
