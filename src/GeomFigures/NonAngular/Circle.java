package GeomFigures.NonAngular;

import GeomFigures.GeomFigure;
import GeomFigures.Point;

public class Circle extends GeomFigure {
    private double radius;

    private Point center;

    private String color;

    @Override
    public double getPerimeter() {
        return 2*Math.PI*radius;
    }

    @Override
    public double getArea() {
        return Math.PI*radius*radius;
    }

    public void setColor(String new_color) {
        this.color = new_color;
    }

    public Circle(double radius, double centerX, double centerY, String color) {
        this.radius = radius;
        this.center = new Point(centerX, centerY);
        this.color = color;
    }

    @Override
    public String toString() {
        return "Point.Circle{" +
                "radius=" + radius +
                ", centerX=" + center.getX() +
                ", centerY=" + center.getY() +
                ", color='" + color + '\'' +
                '}';
    }

}

