package GeomFigures;

public class Point {
    private double x = 0;
    private double y = 0;

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }
}

