package GeomFigures;

public abstract class GeomFigure {
    public abstract double getPerimeter();
    public abstract double getArea();

}